module.exports = function(grunt) {

    //RUN EMBEDDED SERVER 
   grunt.initConfig({
 
	    'http-server': {
	 
	        'dev': {
	            root: "/app",
	            port: 8080,
	            host: "127.0.0.1",
	            showDir : true,
	            autoIndex: true,
	            cache: 0,
	            ext: "html",
	            openBrowser : true
	        }
	    }
	});

    grunt.loadNpmTasks('grunt-http-server');
  	

};