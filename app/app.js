'use strict';
//INIT
var ngDemoApp = angular.module('ngDemoApp', ['ui.router']);
//ROUTING
ngDemoApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/app');
    $stateProvider     
    .state('app', {
        url: '/app',
        templateUrl: 'views/list/listView.html',
        controller: 'ListController'
    })   
    .state('detail', {
        url: '/detail',
        templateUrl: 'views/detail/detailView.html',
        controller: 'DetailController',
        params: {userID: null}
    })         
});