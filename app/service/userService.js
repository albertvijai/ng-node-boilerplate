'use strict';

angular.module('ngDemoApp').factory('UserService', ['$http','$q', function($http, $q){

	console.log('USER SERVICE INITIALIZED');

    //USER SERVICE
	var userService = 'http://jsonplaceholder.typicode.com/users';

    //SERVICE FACTORY
	var factory = {
		getUser:     getUser,
		getUserList: getUserList
	};
	return factory;

    //GET ALL
	function getUserList(){
		var deferred = $q.defer();
		$http
		.get(userService)
		.then(
			function(response){
				deferred.resolve(response.data);
			},
			function(error){
				deferred.reject(error);
			} 
		);
		return deferred.promise;
	}

	//GET A USER 
	function getUser(userID){
		var deferred = $q.defer();
		$http
		.get(userService+'/'+userID)
		.then(
			function(response){
				deferred.resolve(response.data);
			},
			function(error){
				deferred.reject(error);
			} 
		);
		return deferred.promise;
	}

}]);