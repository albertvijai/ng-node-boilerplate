'use strict';

angular.module('ngDemoApp').component('address', {
    template: '<small>{{$ctrl.val.street +", "+$ctrl.val.suite}}</small><br/><small>{{$ctrl.val.city +", "+$ctrl.val.zipcode}}</small>', 
    bindings: {val:'<'}
});