'use strict';

angular.module('ngDemoApp').controller('DetailController', ['$scope', 'UserService', '$state', '$stateParams', 
    function($scope, UserService, $state, $stateParams){

	console.log('DETAIL CONTROLLER INITIALIZED');

    //PAGE VARS 
	var self = this;
	$scope.user = {};
    $scope.userID = $stateParams.userID;
   
    $scope.init = function(){
        if(null!=$scope.userID) loadCurrentUser();
    }

    //FUNCTIONS
    function loadCurrentUser(){
    	UserService.getUser($scope.userID)
    	.then(
    		function(currentUser){
    			$scope.user = currentUser;
    			//console.log('User Detail is '+JSON.stringify(currentUser));
    		},
    		function(error){
    			console.error('User Detail is Empty');
    		}
    	);
    }

}]);