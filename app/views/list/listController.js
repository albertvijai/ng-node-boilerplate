'use strict';

angular.module('ngDemoApp').controller('ListController', ['$scope', 'UserService', function($scope, UserService){

	console.log('LIST CONTROLLER INITIALIZED');

    //PAGE VARS 
	var self = this;
	$scope.userList = [];

    //PAGE EVENTS
    $scope.init = function(){
    	console.log('LOAD USER LIST');
    	$scope.userList = loadUserList();
    }

    //FUNCTIONS
    function loadUserList(){
    	UserService.getUserList()
    	.then(
    		function(userList){
    			$scope.userList = userList;
    			//console.log('User list is '+JSON.stringify(userList));
    		},
    		function(error){
    			console.error('User List is Empty');
    		}
    	);
    }

}]);